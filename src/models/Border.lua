---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by stefan.
--- DateTime: 20/12/2020 10:29
---

Border = Class{}

function Border:init(x, y, width, height, moving)
    self.x = x
    self.y = y
    self.width = width
    self.height = height
    self.moving = moving
end

function Border:update(dt)
    if (self.moving) then
        self.x = self.x - SCROLL_SPEED * dt
    end
end

function Border:draw()
    love.graphics.setColor(0, 0, 0)
    love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
end