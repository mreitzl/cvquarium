---
--- Generated by EmmyLua(https://github.com/EmmyLua)
--- Created by Marcus.
--- DateTime: 19.12.2020 21:51
---


Coral = Class{}


local CORAL_SCALING_FACTOR = 0.15
local CORAL_MOVEMENT_SPEED = 100

local coral_img = love.graphics.newImage("assets/coral.png")
local coral_img_width =coral_img:getWidth()*CORAL_SCALING_FACTOR
local coral_img_height = coral_img:getHeight()*CORAL_SCALING_FACTOR


function Coral:init(x, y)

    -- pos coordinates
    self.x = x
    self.y = y

end

function Coral:update(dt)

    self.x = self.x - CORAL_MOVEMENT_SPEED*dt
end

function Coral:draw()
    love.graphics.setColor(255,255,255)
    love.graphics.draw(coral_img, self.x, self.y, 0, CORAL_SCALING_FACTOR)

end



function Coral:getWidth()
    return coral_img_width
end

function Coral:getHeight()
    return coral_img_height
end
